## Updating of resume.pdf
Each time we update the resume, we will have to convert that to pdf

## Deployment
We are using [Git-Ftp](https://github.com/git-ftp/git-ftp) to sync all our changes to the server. To configure, follow the steps as illustrated in the git repo and set the credentials for git-ftp.url, git-ftp.user & git-ftp.password as advised